import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Content 1.1


// Page to import files from another app in wifiTransfer app.
//   Imported files will be store in the app folder which is used for ftp
//   This will allow ftp sending of files to remote computer via wifi

Page {
    id: importPage
    visible: false

    //tranfer that will be set by ContentPeerPicker
    property var cTransfer

    header: PageHeader {
        id: importHeader
        title: i18n.tr('Select Files to transfer')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }
    }

    // Define a ContentStore to store the imported files
    ContentStore {
        id: fileStore
        scope: ContentScope.App
    }

    ContentPeerPicker {
        anchors.fill: parent
        visible: parent.visible
        showTitle: false

        contentType: ContentType.All            // any type of files
        handler: ContentHandler.Source          // to import file from another app

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Multiple   // allow multiple files
            importPage.cTransfer = peer.request(fileStore)  // request transfer and storage in fileStore
        }

        onCancelPressed: {
            pageStack.pop()
        }
    }

    // Display a waiting screen during transfer
    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: importPage.cTransfer
    }

    Connections {
        target: importPage.cTransfer
        onStateChanged: {

            switch (importPage.cTransfer.state) {
                case ContentTransfer.Created:
                    console.log("Transfer Created")
                    break;

                case ContentTransfer.Initiated:
                    console.log("Transfer Initiated")
                    break;

                case ContentTransfer.InProgress:
                    console.log("Transfer InProgress")
                    break;

                case ContentTransfer.Downloading:
                    console.log("Transfer Downloading")
                    break;

                case ContentTransfer.Downloaded:
                    console.log("Transfer Downloaded")
                    break;

                case ContentTransfer.Charged:
                    console.log("Transfer Charged")
                    for (var i = 0; i < importPage.cTransfer.items.length; i++)
                        console.log("Tranfered file url #", i, importPage.cTransfer.items[i].url)

                    break;

                case ContentTransfer.Collected:
                    console.log("Transfer Collected")
                    importPage.cTransfer.finalize()
                    pageStack.pop()
                    break;

                case ContentTransfer.Aborted:
                    console.log("Transfer Aborted")
                    break;

                case ContentTransfer.Finalized:
                    console.log("Transfer Finalized")
                    break;

                default:
                    console.log("Transfer Unkonwn State")
            }
        }
    }
}
