import QtQuick 2.12
import Lomiri.Components 1.3

Page {
    id: aboutPage
    visible: false

    header: PageHeader {
        id: aboutHeader
        title: i18n.tr('About')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }
    }

    Flickable {
        anchors.top: aboutHeader.bottom
        width: parent.width
        height: parent.height - aboutHeader.height
        contentHeight: column2.height

        Column {
            id: column2
            spacing: units.gu(1)
            topPadding: units.gu(2)
            anchors {
                margins: units.gu(3)
                horizontalCenter: parent.horizontalCenter
            }
            width: parent.width - units.gu(4)

            // Application Name
            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                textSize: Label.Large
                text: "WifiTransfer"
            }

            // Application Version
            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                textSize: Label.Medium
                text: i18n.tr("Version %1").arg("1.2")      // VERSION
            }

            Grid {
                anchors.left: parent.left
                //anchors.leftMargin: units.gu(4)
                columns: 2
                spacing: units.gu(1)

                Label {
                    width: 0.2*column2.width
                    font.bold: true
                    text: i18n.tr("v1.2:")
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.7*column2.width
                    text: i18n.tr("<p>Updates by Aloys Liska:</p>") +
                            i18n.tr("<p>TODO.</p>") +
                            i18n.tr("Source code and issues: ") + "<a href=\"https://gitlab.com/ubuntu-touch-applications/app-forks/wifitransfer\">GitLab</a>"
                    onLinkActivated: Qt.openUrlExternally(link)
                    linkColor: "#c1c101"
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.2*column2.width
                    font.bold: true
                    text: i18n.tr("v1.1:")
                    wrapMode: Text.Wrap
                }


                Label {
                    width: 0.7*column2.width
                    text: i18n.tr("<p>Improve app layout. Add sharing of files via content hub.</p>")
                    wrapMode: Text.Wrap
                }


                Label {
                    width: 0.2*column2.width
                    font.bold: true
                    text: i18n.tr("v1.0:")
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.7*column2.width
                    text: i18n.tr("<p>Updates by Aloys Liska: port app to Focal (20.04).</p>")
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.2*column2.width
                    font.bold: true
                    text: i18n.tr("v0.9:")
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.7*column2.width
                    text: i18n.tr("Version released by Costa Davide for Xenial (16.04).")
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.2*column2.width
                    font.bold: true
                    text: "v0.8:"
                    wrapMode: Text.Wrap
                }

                Label {
                    width: 0.7*column2.width
                    text: i18n.tr("<p>Original version from Stuart Langridge.</p>") +
                            i18n.tr("<p>Many thanks to the checkbox project for providing an example of how to use Python in an Ubuntu SDK app, mzanetti for content hub help, and Dustin MacDonald for the icon.</p>") +
                            i18n.tr("<p>Many thanks also to pyotherside from Thomas Perl, pyzeroconf from William McBrine, and pyftpdlib from Giampaolo Rodolà without which this app couldn’t exist.</p>") +
                            i18n.tr('<p>WifiTransfer is an <a href="http://www.kryogenix.org">sil</a> thing.</p>')
                    onLinkActivated: Qt.openUrlExternally("http://www.kryogenix.org")
                    linkColor: "#c1c101"
                    wrapMode: Text.Wrap
                }
            }
        }
    }
}
