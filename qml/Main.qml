import QtQuick 2.12
import io.thp.pyotherside 1.5
import Lomiri.Components 1.3


MainView {
    id: root
    objectName: 'mainView'

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: 'wifitransfer.aloysliska'

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    automaticOrientation: false

    width: units.gu(40)
    height: units.gu(71)
    backgroundColor: "#6A69A2"
    footerColor: "#8896D5"

    //----------------------
    // Application settings
    //----------------------
    // url of file to export via Content Hub
    property string fileUrltoExport: ""

    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push(Qt.resolvedUrl("wifiTransferPage.qml"))
        }
    }

    // Reception of python events and data are centralized below in py
    // signals are used to redirect events to qml object (with Connections to the signal)
    signal pyfsStarted(var lblParam)
    signal pyfsStopped()
    signal pyfsConnected(bool lblconnectedStatus)
    signal pyfsFileReceived(var pyFileNames)
    signal pyfsIsLocalIP(bool pyLocalIP)

    Python {
        id: py
        Component.onCompleted: {
            // Add the directory of this .qml file to the search path
            addImportPath(Qt.resolvedUrl('../src'));
            importModule('fserver', function () {
                console.log("Python module loaded");
            });

            // To check if local IP present when starting
            py.call("fserver.checkLocalIP", function() {
                console.log("fserver.checkLocalIP returns");
            });
        }
        onError: console.log('Python error: ' + traceback)

        // TODO: may be better to use  setHandler(...)?
        onReceived: {
            console.log("got python data", data);
            if (Array.isArray(data) && data[0] == "started") {
                root.pyfsStarted(data)
            }
            else if (data == "stopped") {
                root.pyfsStopped()
            }
            else if (Array.isArray(data) && data[0] == "files") {
                root.pyfsFileReceived(data)
            }
            else if (Array.isArray(data) && data[0] == "user_connected") {
                root.pyfsConnected(true)
            }
            else if (Array.isArray(data) && data[0] == "user_disconnected") {
                root.pyfsConnected(false)
            }
            else if (Array.isArray(data) && data[0] == "is_local_ip") {
                root.pyfsIsLocalIP(data[1])
            }
        }
    }
}

