import QtQuick 2.12
import Lomiri.Components 1.3

Page {
    id: helpPage
    visible: false

    header: PageHeader {
        id: helpHeader
        title: i18n.tr('Help')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }
    }

    Flickable {
        anchors.top: helpHeader.bottom
        width: parent.width
        height: parent.height - helpHeader.height
        contentHeight: column1.height

        Column {
            id: column1
            spacing: units.gu(2)
            topPadding: units.gu(2)
            anchors {
                margins: units.gu(3)
                horizontalCenter: parent.horizontalCenter
            }
            width: parent.width - units.gu(4)

            Label {
                id: label1
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>WifiTransfer</b> uses File Transfer Protocol (FTP) to transfer files through Wi-Fi. FTP is a way to transfer files between devices over the network.<br> On your Ubuntu Touch (UT) device, WifiTransfer runs an FTP server.<br> On your remote computer, you need to simply run an FTP client and connect to the FTP server.<br> Connection requires a username and a password which are automatically generated for you by WifiTransfer. Once an FTP connection is established, you can transfer files between the two devices.")
            }

            Label {
                id: label2
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>To receive files from your remote computer:</b> WifiTransfer has a <i>shared folder</i> on your Ubuntu Touch (UT) device which is visible from your FTP client on your remote device. Simply add files to this <i>shared folder</i> to transfer them to your UT device. Then, back on your UT device, click on a file to share it with another app (ie. Gallery, File Manager,...).")
            }

            Label {
                id: label3
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>To send files to your remote device:</b> On your Ubuntu Touch (UT) device, with WifiTransfer app open and connected to the remote device, select the file(s) with the 'up' icon located in the top left corner of the app. WifiTransfer will make a copy of selected files in the subfolder <i>Contents</i> of the <i>share folder</i> and the file(s) will be automatically transferred to your remote device.  Note 1: You may need to refresh your device to see the folder and files populate. Note 2: Don't forget to move the files from the <i>Contents</i> directory to a differnt (permanent) location your remote machine, because after the FTP connection is closed a new connection will be required to get them again.")
            }

            Label {
                id: label4
                width: parent.width
                textSize: Label.Large
                wrapMode: Text.Wrap
                text: i18n.tr("<b>Security warning:</b>")
            }

            Label {
                id: label5
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                text: i18n.tr("<b>Disclaimer: FTP is not secure</b>. FTP transmits data, including usernames and passwords, in plain text. This means that anyone intercepting the data can easily read it. Be careful with any sensitive data.  Always disconnect when not in use.")
            }
        }
    }
}
