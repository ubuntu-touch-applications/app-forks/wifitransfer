import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem

Page {
    id: wifitransferPage
    visible: false

    header: PageHeader {
        id: wifiTransferHeader
        title: i18n.tr('WifiTransfer')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }

        leadingActionBar {
            id: mainHeaderActionBar
            actions: [
                Action {
                    iconName: "info"
                    text: i18n.tr('About')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("aboutPage.qml"))
                    }
                },

                Action {
                    iconName: "help"
                    text: i18n.tr('Help')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("helpPage.qml"))
                    }
                },

                Action {
                    iconSource: Qt.resolvedUrl("../assets/export_wifitransfer.svg")
                    text: i18n.tr('Received files')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("importPage.qml"))
                    }
                },

                Action {
                    iconSource: Qt.resolvedUrl("../assets/import_wifitransfer.svg")
                    text: i18n.tr('Imported Files')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("filesPage.qml"))
                    }
                }
            ]
            numberOfSlots: 4
        }

    }


    Flickable {
        anchors.top: wifiTransferHeader.bottom
        width: parent.width
        height: parent.height - wifiTransferHeader.height
        contentHeight: wfcolumn.height

        Column {
            id: wfcolumn
            spacing: units.gu(1)
            anchors {
                margins: units.gu(1)
                top: parent.top
                horizontalCenter: parent.horizontalCenter
            }
            width: parent.width - units.gu(4)

            Rectangle {
                width: parent.width
                height: units.gu(1)
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
            }

            Rectangle {
                width: parent.width
                height: units.gu(1)
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
            }

            Button {
                id: btn
                objectName: "button"
                width: parent.width
                gradient: LomiriColors.orangeGradient

                states: [
                    State {
                        name: "started"
                        PropertyChanges { target: btn; text: i18n.tr("Turn off WifiTransfer"); enabled: true }
                    },
                    State {
                        name: "starting"
                        PropertyChanges { target: btn; text: i18n.tr("(starting)"); enabled: false }
                    },
                    State {
                        name: "stopping"
                        PropertyChanges { target: btn; text: i18n.tr("(stopping)"); enabled: false }
                    },
                    State {
                        name: "stopped"
                        PropertyChanges { target: btn; text: i18n.tr("Turn on WifiTransfer"); enabled: true }
                    }
                ]
                text: i18n.tr("(initialising)")

                onClicked: {
                    if (btn.state == "stopped") {
                        btn.state = "starting";
                        if (py.call_sync("fserver.checkLocalIP") == false) {
                            btn.state = "stopped"
                            lblnolocalip.showing = true
                        }
                        else {
                            py.call("fserver.start", function() {
                                console.log("fserver.start returns");
                            });
                        }
                    } else if (btn.state == "started") {
                        btn.state = "stopping";
                        py.call_sync("fserver.stop");
                    }
                }

                Component.onCompleted: {
                    state = "stopped";
                }
            }

            Rectangle {
                id: lblnolocalip
                width: parent.width
                property bool showing: false
                height: showing ? lblnolocaliptext.height * 4 : 0
                clip: true
                Behavior on height { PropertyAnimation {} }
                color: Qt.rgba(255,255,255,0.2)

                Label {
                    id: lblnolocaliptext
                    text: i18n.tr("Before turning on WifiTransfer, enable Wi-Fi and connect your phone to your Wi-Fi network.")
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    textSize: Label.Large
                    wrapMode: Text.Wrap
                }
            }

            Rectangle {
                id: lblconnected
                width: parent.width
                property bool showing: false
                height: showing ? lblconnectedtext.height * 4 : 0
                clip: true
                Behavior on height { PropertyAnimation {} }
                color: Qt.rgba(255,255,255,0.2)

                Label {
                    id: lblconnectedtext
                    text: i18n.tr("Connected")
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    textSize: Label.Large
                }
            }

            Rectangle {
                width: parent.width
                height: units.gu(1)
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
            }

            Label {
                visible: btn.state == "started"
                horizontalAlignment: Text.AlignLeft
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                textFormat: Text.RichText
                text: i18n.tr("From your computer, use a ftp client to connect with:")
            }

            Label {
                visible: btn.state == "started"
                horizontalAlignment: Text.AlignHCenter
                width: parent.width
                textSize: Label.Medium
                font.italic: true
                wrapMode: Text.Wrap
                textFormat: Text.RichText
                text: "ftp://ubuntu@" + lblip.ip + ":" + lblport.port + "/"
            }

            Rectangle {
                width: parent.width
                height: units.gu(1)
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
            }

            Label {
                visible: btn.state == "started"
                horizontalAlignment: Text.AlignLeft
                width: parent.width
                textSize: Label.Medium
                wrapMode: Text.Wrap
                textFormat: Text.RichText
                text: i18n.tr("Connection details:")
            }

            Grid {
                width: parent.width
                columns: 2
                spacing: units.gu(1)
                leftPadding: units.gu(4)
                visible: btn.state == "started"


                Label {
                    textSize: Label.Medium
                    text: i18n.tr("IP address:")
                }

                Label {
                    id: lblip
                    property string ip
                    horizontalAlignment: Text.AlignLeft
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    font.bold: true
                    text: ip
                }

                Label {
                    textSize: Label.Medium
                    text: i18n.tr("Port:")
                }

                Label {
                    id: lblport
                    property string port
                    horizontalAlignment: Text.AlignLeft
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    font.bold: true
                    text: port
                }

                Label {
                    textSize: Label.Medium
                    text: i18n.tr("username:")
                }

                Label {
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    font.bold: true
                    text: i18n.tr("ubuntu")
                }

               Label {
                    textSize: Label.Medium
                    text: i18n.tr("password:")
                }

               Label {
                    id: lblpwd
                    property string password
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    font.bold: true
                    text: password
                }
            }

            Rectangle {
                width: parent.width
                height: units.gu(1)
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
            }

            Grid {
                columns: 2
                spacing: units.gu(2)

                Label {
                    horizontalAlignment: Text.AlignLeft
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    text: i18n.tr("Local shared folder:")
                }

                Label {
                    id: lblfolder
                    width: wifiTransferHeader.width / 2
                    visible: btn.state == "started"
                    property string folder
                    horizontalAlignment: Text.AlignLeft
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    font.italic: true
                    text: {
                        var outtext3;
                        if (folder == "all") {
                            outtext3 = "all your files";
                        } else {
                            outtext3 = folder;
                        }
                        return outtext3;
                    }
                }
            }

            Grid {
                columns: 2
                spacing: units.gu(2)

                Image {
                    id: recIcon
                    width: units.gu(4)
                    height: units.gu(4)
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../assets/import_wifitransfer.svg")
                    TapHandler {
                        onTapped: pageStack.push(Qt.resolvedUrl("filesPage.qml"))
                    }
                }

                Label {
                    width: wfcolumn.width - recIcon.width - units.gu(2)
                    horizontalAlignment: Text.AlignLeft
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    text: i18n.tr("<b>Receiving</b>:") + "<br>" +
                          i18n.tr("Access to received files from your computer and share them with another app (Gallery, File Manager,...)")
                }

                Image {
                    id: sendIcon
                    width: units.gu(4)
                    height: units.gu(4)
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../assets/export_wifitransfer.svg")
                    TapHandler {
                        onTapped: pageStack.push(Qt.resolvedUrl("importPage.qml"))
                    }
                }

                Label {
                    width: wfcolumn.width - sendIcon.width - units.gu(2)
                    horizontalAlignment: Text.AlignLeft
                    textSize: Label.Medium
                    wrapMode: Text.Wrap
                    text: i18n.tr("<b>Sending</b>:") + "<br>" +
                          i18n.tr("Select files to be sent to your computer (they will appear in <i>Contents</i> subfolder).")
                }
            }

            // Manages signal received from root (in Main.qml, coming from python)
            Connections {
                target: root
                onPyfsStarted: {
                    btn.state = "started"
                    lblip.ip = lblParam[1]
                    lblport.port = lblParam[2]
                    lblfolder.folder = lblParam[3]
                    lblpwd.password = lblParam[4]
                    lblnolocalip.showing = false
                }
            }

            Connections {
                target: root
                onPyfsStopped: {
                    btn.state = "stopped"
                    lblconnected.showing = false
                }
            }

            Connections {
                target: root
                onPyfsConnected: {
                    lblconnected.showing = lblconnectedStatus
                }
            }

            Connections {
                target: root
                onPyfsIsLocalIP: {
                    if (pyLocalIP == false) {
                        lblnolocalip.showing = true
                    }
                    else {
                        lblnolocalip.showing = false
                    }
                }
            }
        }
    }
}
