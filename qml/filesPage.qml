import QtQuick 2.12
import Qt.labs.folderlistmodel 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem


Page {
    id: filesPage
    visible: false

    header: PageHeader {
        id: filesHeader
        title: i18n.tr('Files')
        StyleHints {
            backgroundColor: "#343C60"
            dividerColor: LomiriColors.slate
        }
    }

    FolderListModel {
        id: filesModel
        sortField: FolderListModel.Name
        showDirsFirst: true
        showDotAndDotDot: true
    }

    ListView {
        id: filesView
        model: filesModel
        clip: true
        anchors.fill: parent
        delegate: ListItem.Standard {
            id: fileDelegate
            iconName: fileName == ".." ? "up" : (fileIsDir ? "folder" : "share")
            iconFrame: false
            text: fileName == ".." ? "up" : fileName
            visible: fileName != "."
            onClicked: {
                if (fileIsDir) {
                    if (fileName == "..") {
                        var parts = filesModel.folder.toString().split("/");
                        filesModel.folder = parts.slice(0, parts.length - 1).join("/")
                    }
                    else {
                        filesModel.folder = filesModel.folder + "/" + fileName
                    }
                }
                else {
                    console.log("content hub file", filesModel.folder + "/" + fileName);
                    fileUrltoExport = filesModel.folder + "/" + fileName;
                    pageStack.push(Qt.resolvedUrl("exportPage.qml"))
                }
            }
        }
    }

    // Manages signal received from root (in Main.qml, coming from python)
    Connections {
        target: root
        onPyfsFileReceived: {
            // append filename and full path name
            filesModel.append({fn: pyFileNames[1], ffn: pyFileNames[2]})
        }
    }

    Component.onCompleted: {
        if (!filesModel.rootfolder) {
            filesModel.rootFolder = py.call_sync("fserver.getfolder")
            filesModel.folder = filesModel.rootFolder
        }
   }
}
