#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside, os, socket, random, string

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

from zeroconf import ServiceInfo, Zeroconf

#----------------------------------------------------------------------------------------
# To debug, insert a pyotherside.send(event) in python code
# the event is catch in main.QML to display it with console.log("got python data", data)
# Example:
#  pyotherside.send("my_PYTHON_DEBUG", APP_ID)
#----------------------------------------------------------------------------------------

server = None
stopRequest = True

#----------------------------------------------------------------------------------------
def get_writable_dir():

    # Get identifier of app. Debug observation:
    #    - with clickable desktop: os.environ.get("APP_ID") return empty, so APP_ID="wifitransfer.aloysliska_ignored"
    #    - with phone: it returns wifitransfer.aloysliska_wifitransfer_1.0.x (where 1.0.x is version number)
    APP_ID = os.environ.get("APP_ID", "wifitransfer.aloysliska_ignored")

    APP_PKGNAME = APP_ID.split('_')[0]      # expect to be: wifitransfer.aloysliska

    # Get app home data folder. Debug observation:
    #   - with clickable desktop: XDG =         (empty)
    #   -               on phone: XDG = /home/phablet/.local/share
    XDG = os.environ.get('XDG_DATA_HOME')

    if not XDG:
        my_writable_dir = "."
    else:
        if not XDG.endswith("/"): XDG = XDG + "/"
        my_writable_dir = XDG + APP_PKGNAME

    # Get Home folder name. Debug observation:
    # - clickable desktop: HOME = /home/phablet
    # -          on phone: HOME = /home/phablet
    HOME = os.path.expanduser("~")
    if not HOME.endswith("/"): HOME = HOME + "/"

    # try writing to $HOME; if it works, make that our folder
    try:
        test_home_file = os.path.join(HOME, "wifitransfer.tmp")
        fp = open(test_home_file, "w")
        fp.write("can we write to $HOME?")
        fp.close()
        os.unlink(test_home_file)
        my_writable_dir = HOME
        my_writable_dir_short = "all"
    except:
        # here we have previous affectation: my_writable_dir = XDG + APP_PKGNAME
        my_writable_dir_short = os.path.abspath(my_writable_dir).replace(HOME, "")

    # Create directory my_writable_dir if it does not exists
    if not os.path.isdir(my_writable_dir):
        os.mkdir(my_writable_dir)

    # Debug Observations:
    # clickable desktop:
    #    - my_writable_dir = /home/phablet/
    #    - my_writable_dir_short = all
    # on phone:
    #    - my_writable_dir = /home/phablet/.local/share/wifitransfer.aloysliska
    #    - my_writable_dir_short = .local/share/wifitransfer.aloysliska]

    return my_writable_dir, my_writable_dir_short


#----------------------------------------------------------------------------------------
class EventFiringFTPHandler(FTPHandler):
    def on_login(self, username):
        pyotherside.send("user_connected", username, self.remote_ip, self.remote_port)

    def on_logout(self, username):
        pyotherside.send("user_disconnected", username)

    def on_file_received(self, ffn):
        # do something when a file has been received
        pyotherside.send("files", os.path.split(ffn)[1], ffn)

    def on_incomplete_file_received(self, file):
        # remove partially uploaded files
        import os
        os.remove(file)


#----------------------------------------------------------------------------------------
def start_ftpserver():
    global server
    global stopRequest
    global zeroconf, zeroconf_info

    # Instantiate a dummy authorizer for managing 'virtual' users
    authorizer = DummyAuthorizer()

    # Get a folder for the FTP server
    my_writable_dir, my_writable_dir_short = get_writable_dir()

    # Generate a random password with 5 characters
    thepassword = "".join([random.choice(string.ascii_lowercase) for i in range(5)])

    # Define user
    authorizer.add_user("ubuntu", thepassword, my_writable_dir, perm="elradfmw")

    # Instantiate FTP handler class
    handler = EventFiringFTPHandler
    handler.authorizer = authorizer
    #handler.auth_failed_timeout = 0.001
    # lower buffer sizes = more "loops" while transferring data
    # = less false positives
    handler.dtp_handler.ac_in_buffer_size = 4096
    handler.dtp_handler.ac_out_buffer_size = 4096

    # Define a customized banner (string returned when client connects)
    handler.banner = "WifiTransfer - pyftpdlib based FTP server ready."

    #Get local IP with socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    local_ip = s.getsockname()[0]
    s.close()

    # Create server
    server = FTPServer((local_ip,0), handler)
    host, port = server.socket.getsockname()[:2]

    # Zeroconf
    zeroconf = Zeroconf()
    desc = {"u":"ubuntu"}
    zeroconf_info = ServiceInfo("_ftp._tcp.local.",
                        "WifiTransfer on Ubuntu phone._ftp._tcp.local.",
                        addresses = [socket.inet_aton(local_ip)],
                        port = port,
                        properties = desc,
                        server=socket.gethostname() + ".local.")

    zeroconf.register_service(zeroconf_info)


    pyotherside.send("started", host, port, my_writable_dir_short, thepassword)

    # start ftp server
    stopRequest = False
    while not stopRequest:
        server.serve_forever(timeout=1.0, blocking=False, handle_exit=True)


#----------------------------------------------------------------------------------------
def stop_ftpserver():
    global server
    global zeroconf, zeroconf_info
    server.close_all()
    zeroconf.unregister_service(zeroconf_info)
    zeroconf.close()

#----------------------------------------------------------------------------------------
def checkLocalIP():
    # Local IP addresses. Address starting by '10.' is not considered local because this may be address with mobile data
    local_ips = ['127.0.0.1', 'localhost']
    local_ip_range = ['192.168.', '172.16.', '172.17.', '172.18.', '172.19.', '172.20.', '172.21.', '172.22.', '172.23.', '172.24.', '172.25.', '172.26.', '172.27.', '172.28.', '172.29.', '172.30.', '172.31.']

    # Get local IP with socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
    except OSError as e:
        # in case no network
        is_local = False
    finally:
        detected_ip = s.getsockname()[0]
        s.close()
        # Check if detected IP is local
        is_local = detected_ip in local_ips or any(detected_ip.startswith(ip) for ip in local_ip_range)

    pyotherside.send("is_local_ip", is_local)

    return is_local


#----------------------------------------------------------------------------------------
def getfolder():
    my_writable_dir, my_writable_dir_short = get_writable_dir()
    pyotherside.send("writeabledir", my_writable_dir, my_writable_dir_short)
    return my_writable_dir

#----------------------------------------------------------------------------------------
def start():
    pyotherside.send("starting...")
    start_ftpserver()

#----------------------------------------------------------------------------------------
def stop():
    global server
    global stopRequest
    pyotherside.send("stopping...")
    stopRequest = True
    stop_ftpserver()
    pyotherside.send("stopped")
    server = None


