
# WifiTransfer
WifiTransfer is an application to transfer files to and from your Ubuntu Touch phone with FTP.

A simple FTP server for Ubuntu Touch devices; start it and then connect to your device from your normal FTP application on your computer (the file manager, Filezilla, or any other) using the connection details provided.

# App Architecture
- QML
   - `Main.qml`:define a PageStack pushing `wifiTransfer.qml`
   - `wifiTransfer.qml`: the core page to enable/disable ftp server and to display connections informations
      - on user actions push `filesPage.qml`, `importPage`, `helpPage` or `aboutPage.qml`
   - `filesPage.qml`: display files in application folder `.local/share/wifitransfer/aloysliska` which is the share folder by FTP.
     Received files from remote computer are stored in this folder. A file can be exported to another app via Content Hub.
      - `exportPage.qml`: to export with content hub a file selected by user in `filesPage.qml`
   - `importPage.qml`: to import with content hub files from another app, so that the files are stored in share folder and so sent to remote computer by FTP.
   - `helpPage.qml`: page to show a brief explaination of FTP server, file reception or file sending
   - `aboutPage.qml`: information about the app

- python code and libraries `pyftpdlib` and `python-zeroconf`
  - fserver.py
    - functions which are called from QML with pyotherside:
      - start(): start the FTP server using pyftpdlib and register service with python-zeroconf
      - stop(): stop the FTP server
      - getfolder(): find a writable folder which will be the folder shared through FTP
      - checkLocalIP(): detect if ip is a local one
    - events sent from python to QML with pyotherside:
      - started: FTP server is started
      - user_connected: user log in
      - user_disconnected: user log out
      - files: files received from FTP client
      - is_local_ip: true is ip is local, false otherwise
  - dependencies:
    - pyftpdlib: python FTP server library, this is a fork of https://github.com/giampaolo/pyftpdlib/
    - python-zeroconf: python library that enables service discovery and registration for devices in a local network without needing a central server, using the Zeroconf/Bonjour/Avahi protocols. This is a fork of https://github.com/python-zeroconf/python-zeroconf
    - ifaddr (required by python-zeroconf): small Python library that allows you to find all the Ethernet and IP addresses of the computer. This is a fork of https://github.com/ifaddr/ifaddr
    - async-timeout (required by python-zeroconf): Python utility that provides context managers for creating timeouts for asynchronous operations in asyncio code. This is a fork of https://github.com/aio-libs/async-timeout



# Author and Contributor
 - Updated by **Aloys Liska** in 2024 for Focal (Ubuntu 20.04)
 - Updated by **Costa Davide** for Xenial (16.04)
 - Original Author: **Stuart Langridge** in 2015-2017. See https://launchpad.net/wifitransfer


# Licence
MIT / X / Expat Licence
- pyftpdlib: MIT, see https://gitlab.com/ubuntu-touch-applications/app-forks/externalcomponents/pyftpdlib/-/blob/d90cc93e850048668c411af7502c81c1ab093fce/LICENSE
- python-zeroconf: LGPL, see https://gitlab.com/ubuntu-touch-applications/app-forks/externalcomponents/python-zeroconf/-/blob/938fe214089d6eb7438f0f03ac19a3a724566d37/COPYING
- ifaddr: MIT, see https://gitlab.com/ubuntu-touch-applications/app-forks/externalcomponents/ifaddr/-/blob/107a5a19b3106be44b7e86ac734a2fc36b93a426/LICENSE.txt
- async-timeout: Apache v2.0, see https://gitlab.com/ubuntu-touch-applications/app-forks/externalcomponents/async-timeout/-/blob/65176cdd81b7a3710a09657c128d460a6f88f7ca/LICENSE



# Original *readme.txt* content (from v0.8)
where did you get the python stuff from, Langridge?

Install checkbox on your phone, then look in com.ubuntu.checkbox/current/lib in the click folder in /opt, which has all the python stuff in and copy it all into your app. Remove lib/py, because that's checkbox's dependencies.

TODO

* Translations (done on LP, but not integrated back into app yet)
* keep screen turned on while server is running and app is foreground
* keep server running while app is in background (not yet possible)
* change design to look like https://dribbble.com/shots/1878610-Monochromatic-calendar
* openstore version
